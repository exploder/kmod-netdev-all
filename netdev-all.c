
/**
 * knetstat
 * Copyright (C) 2013-2019  Andreas Veithen
 * Copyright (C) 2014  Google
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/types.h>
#include <linux/netdevice.h>
#include <linux/slab.h>

#include <net/net_namespace.h>

/*
 *	This is invoked by the /proc filesystem handler to display a device
 *	in detail.
 */
static void *dev_seq_start(struct seq_file *seq, loff_t *pos)
	__acquires(RCU)
{
	rcu_read_lock();

	if (!*pos){
		seq->private = kzalloc(sizeof(struct net*), GFP_KERNEL);
		return SEQ_START_TOKEN;
	}

	return NULL;
}

static void *dev_seq_next(struct seq_file *seq, void *v, loff_t *pos)
{
	++*pos;
	if (v == SEQ_START_TOKEN) {
		seq->private = first_net_device(&init_net);
		return seq->private;
	}
	seq->private = next_net_device(seq->private);
	return seq->private;
}

static void dev_seq_stop(struct seq_file *seq, void *v)
	__releases(RCU)
{
	kfree(seq->private);
	rcu_read_unlock();
}

static void dev_seq_printf_stats(struct seq_file *seq, struct net_device *dev)
{
	struct rtnl_link_stats64 temp;
	const struct rtnl_link_stats64 *stats = dev_get_stats(dev, &temp);

	seq_printf(seq, "%6s: %7llu %7llu %4llu %4llu %4llu %5llu %10llu %9llu "
					"%8llu %7llu %4llu %4llu %4llu %5llu %7llu %10llu\n",
			   dev->name, stats->rx_bytes, stats->rx_packets,
			   stats->rx_errors,
			   stats->rx_dropped + stats->rx_missed_errors,
			   stats->rx_fifo_errors,
			   stats->rx_length_errors + stats->rx_over_errors +
				   stats->rx_crc_errors + stats->rx_frame_errors,
			   stats->rx_compressed, stats->multicast,
			   stats->tx_bytes, stats->tx_packets,
			   stats->tx_errors, stats->tx_dropped,
			   stats->tx_fifo_errors, stats->collisions,
			   stats->tx_carrier_errors +
				   stats->tx_aborted_errors +
				   stats->tx_window_errors +
				   stats->tx_heartbeat_errors,
			   stats->tx_compressed);
}

static int dev_seq_show(struct seq_file *seq, void *v) {
	if (v == SEQ_START_TOKEN) {
		seq_puts(seq, "Inter-|   Receive                            "
					  "                    |  Transmit\n"
					  " face |bytes    packets errs drop fifo frame "
					  "compressed multicast|bytes    packets errs "
					  "drop fifo colls carrier compressed\n");
	} else {
		dev_seq_printf_stats(seq, v);
	}
	return 0;
}


static const struct seq_operations tcpstat_seq_ops = {
	.show		= dev_seq_show,
	.start		= dev_seq_start,
	.next		= dev_seq_next,
	.stop		= dev_seq_stop,
};

/* This function is called when the /proc file is open. */
static int my_open(struct inode *inode, struct file *file)
{
	return seq_open(file, &tcpstat_seq_ops);
};

static const struct proc_ops my_file_ops = {
	.proc_open = my_open,
	.proc_read = seq_read,
	.proc_lseek = seq_lseek,
	.proc_release = seq_release,
};

static int __init procfs4_init(void)
{
    struct proc_dir_entry *entry;

    entry = proc_create("net-dev-all", 0444, NULL, &my_file_ops);
    if (entry == NULL) {
		remove_proc_entry("net-dev-all", NULL);
		pr_debug("Error: Could not initialize /proc/%s\n", "net-dev-all");
		return -ENOMEM;
    }

    return 0;
}

static void __exit procfs4_exit(void)
{
	remove_proc_entry("net-dev-all", NULL);
	pr_debug("/proc/%s removed\n", "net-dev-all");
}

module_init(procfs4_init)
module_exit(procfs4_exit)

MODULE_LICENSE("GPL");